package assign.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Count {
	public String year;
	public ArrayList<String> count;
	
	public Count(String year, ArrayList<String> count){
		this.year = year;
		this.count = count;
	}

	public int getYear() {
		return Integer.parseInt(this.year.replace("/",""));
	}

	public int getCount() {
		Set<String> dateSet = new HashSet<String>();
		
		for(String cnt:this.count){
			String[] date_temp = cnt.split("\\.");
			if(date_temp.length > 1){
				String date = date_temp[1];
				String[] date_passed = date.split("-");
				String date_hello = date_passed[0]+"-"+date_passed[1]+"-"+date_passed[2];
				dateSet.add(date_hello);
			}else{
				continue;
			}
		}
		
		return dateSet.size();
	}
}