package assign.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import assign.domain.Count;
import assign.services.EavesdropService;

@Path("/solum")
public class UTCoursesResource {
	
	EavesdropService eavesdropService;
	
	public UTCoursesResource() {
		this.eavesdropService = new EavesdropService();
	}
	
	@GET
	@Path("")
	@Produces("text/html")
	public String helloWorld() throws IOException {
		
		ArrayList<Count> count_list =  new ArrayList<Count>();
		
		String json_creator ="[";
		
		ArrayList<String> years = get_years("solum_team_meeting");
		if(years != null){
			for(String year:years){
				Count here_set;
				ArrayList<String> year_sent = get_UrlData("solum_team_meeting", year);
				here_set = new Count(year, year_sent);
				count_list.add(here_set);
				json_creator += "{\"year\":\""+here_set.getYear()+"\", \"count\":\""+here_set.getCount()+"\"},";
				System.out.println("Year: "+here_set.getYear()+" : "+here_set.getCount());
			}
		}
		if (json_creator != null && json_creator.length() > 0 && json_creator.charAt(json_creator.length()-1)==',') {
			json_creator = json_creator.substring(0, json_creator.length()-1);
		}
		
		json_creator += "]";
		
		return json_creator;		
	}
	
	public static ArrayList<String> get_UrlData(String project_discriptions, String value_discriptions) throws IOException {
		ArrayList<String> URL_Array = new ArrayList<String>();
		String url_array = "http://eavesdrop.openstack.org/meetings/"+project_discriptions.toLowerCase()+"/"+value_discriptions;
		
		try{
			Document doc = Jsoup.connect(url_array).get();
			Elements headlines = doc.select("tr");
			boolean this_is = false;
			for(Element el : headlines){
				if(el.text().contains("Parent Directory")){
					this_is = true;
					continue;
				}
				if(this_is){
					Elements td_elements = el.select("td");
					int counter = 0;
					String keep_this = "";
					for(Element elem:td_elements){
						if(counter < 3){
							keep_this += elem.text()+" ";
						}
						counter = counter + 1;
					}
					
					URL_Array.add(keep_this.split(" ")[1]);
				}
			}
			
			return URL_Array;
		}catch(Exception e){
			return URL_Array;
		}
		
	}
	
	//public static ArrayList<Count> count_meetings(String project){
	public static ArrayList<String> get_years(String project){
		ArrayList<String> URL_Array = new ArrayList<String>();
		
		String url_array = "http://eavesdrop.openstack.org/meetings/"+project.toLowerCase();
		
		try{
			Document doc = Jsoup.connect(url_array).get();
			Elements headlines = doc.select("tr");
			boolean this_is = false;
			for(Element el : headlines){
				if(el.text().contains("Parent Directory")){
					this_is = true;
					continue;
				}
				if(this_is){
					Elements td_elements = el.select("td");
					int counter = 0;
					String keep_this = "";
					for(Element elem:td_elements){
						if(counter < 3){
							keep_this += elem.text()+" ";
						}
						counter = counter + 1;
					}
					
					URL_Array.add(keep_this.split(" ")[1]);
				}
			}

		}catch(Exception e){
			return URL_Array;
		}
		
		return URL_Array;
	}
}